#!/bin/bash

while true
do
CAPACITY=`cat /sys/class/power_supply/BAT1/capacity`
STATUS=`cat /sys/class/power_supply/BAT1/status`
#CAPACITY=`acpi -b | grep -P -o '[0-9]+(?=%)'`


if [[ (${CAPACITY} > 90) && (${STATUS} == "Charging")]]
then
/usr/bin/notify-send -i "$HOME/.icons/bat/battery.png" -u critical "Disconnect Charger. Battery is above 90%"
elif [[ (${CAPACITY} < 45) && (${STATUS} == "Discharging")]]
then
/usr/bin/notify-send -i "$HOME/.icons/bat/batterylow.png" -u critical "Connect Charger. Battery Getting LOW!"
fi

sleep 120

done
