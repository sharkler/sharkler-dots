#!/bin/sh
swayidle -w \
                timeout 300 'swaylock -f -i ~/.wallpaper.png' \
                timeout 600 'hyprctl dispatch dpms off' \
                resume 'hyprctl dispatch dpms on' \
                before-sleep 'swaylock -f -i ~/.wallpaper.png'
