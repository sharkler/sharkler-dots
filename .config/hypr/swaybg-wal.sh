#!/bin/bash

wal -q -i ~/Pictures/hyprbg

if [[ $(pidof swaybg) ]]; then
   pkill swaybg
fi

swaybg -m fill -i "$(< "${HOME}/.cache/wal/wal")"

if [[ $(pidof waybar) ]]; then
   killall -SIGUSR2 waybar
fi

/home/void/.config/mako/update-theme.sh

notify-send -i "$(< "${HOME}/.cache/wal/wal")" "Theme Changed"
