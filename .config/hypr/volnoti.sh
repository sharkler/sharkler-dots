#!/bin/bash

VOLUME=`wpctl get-volume @DEFAULT_AUDIO_SINK@`
notify-send -i ~/.icons/bat/volume.png "$VOLUME%"
