#!/bin/bash

wal -q --iterative -i ~/Pictures/hyprbg

swww img "$(< "${HOME}/.cache/wal/wal")" --transition-type random

killall -SIGUSR2 waybar

/home/void/.config/mako/update-theme.sh

notify-send -i "$(< "${HOME}/.cache/wal/wal")" "Theme Changed"

