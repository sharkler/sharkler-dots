#!/bin/bash

wal -q -i ~/Pictures/hyprbg

~/.nix-profile/bin/swww img "$(< "${HOME}/.cache/wal/wal")" --transition-type random

if [[ $(pidof waybar) ]]; then
   killall -SIGUSR2 waybar
fi

/home/void/.config/mako/update-theme.sh

notify-send -i "$(< "${HOME}/.cache/wal/wal")" "Theme Changed"
