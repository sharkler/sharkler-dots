input {
    keyboard {
        xkb {
            layout "us,in"
            variant ",tamilnet"
            options "grp:ctrl_space_toggle"
        }
    }

    // Omitting touchpad settings disables them, or leaves them at their default values.
touchpad {
        tap
        // dwt
        // dwtp
        // natural-scroll
        // accel-speed 0.2
        // accel-profile "flat"
    }

mouse {
        // natural-scroll
        // accel-speed 0.2
        // accel-profile "flat"
    }

    // Uncomment this to make the mouse warp to the center of newly focused windows.
    // warp-mouse-to-focus

    // Focus windows and outputs automatically when moving the mouse into them.
    focus-follows-mouse
}

layout {
    // Set gaps around windows in logical pixels.
    gaps 6
    center-focused-column "never"

preset-column-widths {
        proportion 0.33333
        proportion 0.5
        proportion 0.66667

        // fixed 1920
    }

focus-ring {
        off
    }

border {
        width 2
         active-color "#b88c76"
         inactive-color "#57689b"

       // active-gradient from="#bf616a" to="#f38ba8" angle=45 relative-to="workspace-view"
       // inactive-gradient from="#505050" to="#808080" angle=45 relative-to="workspace-view"
    }
}

workspace "common"
workspace "media"
prefer-no-csd

spawn-at-startup "swww" "init"
spawn-at-startup "pipewire"
spawn-at-startup "gammastep"
spawn-at-startup "waybar" "-c" "/home/void/.config/niri/waybar/config" "-s" "/home/void/.config/niri/waybar/style.css"
spawn-at-startup "nm-applet" "--indicator"
spawn-at-startup "mako"
spawn-at-startup "/usr/libexec/xfce-polkit"
spawn-at-startup "udiskie" "-ans"
spawn-at-startup "batterynotify.sh"
spawn-at-startup "/home/void/.config/niri/swayidle.sh"
spawn-at-startup "/home/void/.config/niri/swaybg.sh"

hotkey-overlay {
    skip-at-startup
}

environment {
    XDG_CURRENT_DESKTOP "niri"
    XDG_SESSION_TYPE "wayland"
    QT_QPA_PLATFORM "wayland"
    DISPLAY null
    MOZ_ENABLE_WAYLAND "1"
    QT_WAYLAND_DISABLE_WINDOWDECORATION "1"
}

screenshot-path "~/Pictures/Screenshot from %Y-%m-%d %H-%M-%S.png"

// You can also set this to null to disable saving screenshots to disk.
// screenshot-path null

animations {
    slowdown 2.0
    window-open {
      duration-ms 200
      curve "linear"
      custom-shader r"
    
    vec4 expanding_circle(vec3 coords_geo, vec3 size_geo) {
    vec3 coords_tex = niri_geo_to_tex * coords_geo;
    vec4 color = texture2D(niri_tex, coords_tex.st);
    vec2 coords = (coords_geo.xy - vec2(0.5, 0.5)) * size_geo.xy * 2.0;
    coords = coords / length(size_geo.xy);
    float p = niri_clamped_progress;
    if (p * p <= dot(coords, coords))
    color = vec4(0.0);

    return color;
  }

    vec4 open_color(vec3 coords_geo, vec3 size_geo) {
    return expanding_circle(coords_geo, size_geo);
   }
  "
 }

   window-close {
      duration-ms 250
      curve "linear"
      custom-shader r"

    vec4 fall_and_rotate(vec3 coords_geo, vec3 size_geo) {
    float progress = niri_clamped_progress * niri_clamped_progress;
    vec2 coords = (coords_geo.xy - vec2(0.5, 1.0)) * size_geo.xy;
    coords.y -= progress * 1440.0;
    float random = (niri_random_seed - 0.5) / 2.0;
    random = sign(random) - random;
    float max_angle = 0.5 * random;
    float angle = progress * max_angle;
    mat2 rotate = mat2(cos(angle), -sin(angle), sin(angle), cos(angle));
    coords = rotate * coords;
    coords_geo = vec3(coords / size_geo.xy + vec2(0.5, 1.0), 1.0);
    vec3 coords_tex = niri_geo_to_tex * coords_geo;
    vec4 color = texture2D(niri_tex, coords_tex.st);

    return color;
  }

    vec4 close_color(vec3 coords_geo, vec3 size_geo) {
    return fall_and_rotate(coords_geo, size_geo);
     }
    "
   }
}

window-rule {
    geometry-corner-radius 8
    clip-to-geometry true
}

window-rule {
    match app-id=r#"^org\.keepassxc\.KeePassXC$"#
    block-out-from "screen-capture"

    // block-out-from "screencast"
}

window-rule {
    draw-border-with-background false
    match is-active=true
    match is-active=false
    opacity 0.90
}

window-rule {
    match app-id="libreoffice-calc"
    match app-id="org.keepassxc.KeePassXC"
    match app-id="librewolf"
    match app-id="org.pwmt.zathura"
    match app-id="galculator"
    opacity 0.95
}

window-rule {
    match app-id="mpv"
    match title="swayimg"
    opacity 1.0
}

window-rule {
   match app-id="mpv$"
   match app-id="org.qutebrowser.qutebrowser$"
   match app-id="librewolf$"
   open-maximized true
   //match app-id="foot$"
   //match app-id="thunar$"
   //match app-id="geany$"
   // default-column-width { fixed 1300; }
}

window-rule {
    match app-id="org.keepassxc.KeePassXC"
    match app-id="deadbeef"
    match app-id="goodvibes"
    match app-id="de.haeckerfelix.Shortwave"
    match app-id="com.transmissionbt.transmission_2051_3670018"
    match app-id="org.telegram.desktop"
    match app-id="galculator"
    match app-id="com.github.wwmm.easyeffects"
    match app-id="xfce-polkit"
    open-floating true
}

window-rule {
    match app-id="thunar" title="File Operation Progress"
    match app-id="librewolf" title="Opening .*"
    match app-id="librewolf" title="About LibreWolf"
    match app-id="engramba" title="Extract .*"
    open-floating true
}

binds {
    Mod+Shift+Slash { show-hotkey-overlay; }
    Mod+Return { spawn "foot" "fish"; }
    Mod+Shift+Return { spawn "foot"; }
    Mod+W { spawn "fuzzel"; }
    Mod+E { spawn "thunar"; }
    Mod+X { spawn "geany"; }
    Mod+K { spawn "keepassxc"; }
    Mod+Shift+E { spawn "foot" "/home/void/.config/niri/yazi.sh"; }
    Mod+B { spawn "/home/void/.local/bin/librewolf/librewolf"; }
    Mod+Shift+B { spawn "firejail" "qutebrowser"; }
    Mod+T { spawn "telegram-desktop"; }
    Mod+Shift+T { spawn "transmission-gtk"; }
    Mod+D { spawn "bash" "-c" "niri msg action focus-workspace media && deadbeef"; }
    Mod+M { spawn "bash" "-c" "niri msg action focus-workspace media && goodvibes"; }
    Mod+S { spawn "bash" "-c" "niri msg action focus-workspace media && shortwave"; }
    Mod+Shift+M { spawn "bash" "-c" "niri msg action focus-workspace media && easyeffects"; }
    Mod+Ctrl+L { spawn "wlogout"; }
    Super+Alt+L { spawn "bash" "-c" "niri msg action do-screen-transition --delay-ms 350 && swaylock -f -i /home/void/.wallpaper.jpg"; }
    Alt+W { spawn "/home/void/.config/niri/wal.sh"; }
    Alt+B { spawn "/home/void/.config/niri/swww_cyclebg.sh"; }
    Alt+C { spawn "/home/void/.config/niri/cyclecolors.sh"; }
    Alt+S { spawn "bash" "-c" "foot --window-size-pixels=$(slurp -d | cut -d' ' -f2)"; }
    
    // Volume keys mappings for PipeWire & WirePlumber.
    XF86AudioRaiseVolume  allow-when-locked=true { spawn "wpctl" "set-volume" "@DEFAULT_AUDIO_SINK@" "5%+"; }
    XF86AudioLowerVolume allow-when-locked=true { spawn "wpctl" "set-volume" "@DEFAULT_AUDIO_SINK@" "5%-"; }
    XF86AudioMute allow-when-locked=true { spawn "wpctl" "set-mute" "@DEFAULT_AUDIO_SINK@" "toggle"; }
    XF86AudioMicMute allow-when-locked=true { spawn "wpctl" "set-mute" "@DEFAULT_AUDIO_SOURCE@" "toggle"; }
     
    XF86MonBrightnessUp { spawn "brightnessctl" "set" "+5%"; }
    XF86MonBrightnessDown { spawn "brightnessctl" "set" "5%-"; }

    Mod+Q { close-window; }
    
    Mod+v { toggle-window-floating; }
    Mod+Shift+v { switch-focus-between-floating-and-tiling; }

    Mod+Left  { focus-column-left; }
    Mod+Down  { focus-window-down; }
    Mod+Up    { focus-window-up; }
    Mod+Right { focus-column-right; }
    
    Mod+Shift+Left  { focus-column-first; }
    Mod+Shift+Right { focus-column-last; }

    Mod+Ctrl+Left  { move-column-left; }
    Mod+Ctrl+Down  { move-window-down; }
    Mod+Ctrl+Up    { move-window-up; }
    Mod+Ctrl+Right { move-column-right; }

    Mod+U              { focus-workspace-down; }
    Mod+I              { focus-workspace-up; }
    Mod+Ctrl+U         { move-column-to-workspace-down; }
    Mod+Ctrl+I         { move-column-to-workspace-up; }

    Mod+WheelScrollDown      cooldown-ms=150 { focus-workspace-down; }
    Mod+WheelScrollUp        cooldown-ms=150 { focus-workspace-up; }
    Mod+Ctrl+WheelScrollDown cooldown-ms=150 { move-column-to-workspace-down; }
    Mod+Ctrl+WheelScrollUp   cooldown-ms=150 { move-column-to-workspace-up; }

    Mod+WheelScrollRight      { focus-column-right; }
    Mod+WheelScrollLeft       { focus-column-left; }
    Mod+Ctrl+WheelScrollRight { move-column-right; }
    Mod+Ctrl+WheelScrollLeft  { move-column-left; }

    // Usually scrolling up and down with Shift in applications results in
    // horizontal scrolling; these binds replicate that.
    Mod+Shift+WheelScrollDown      { focus-column-right; }
    Mod+Shift+WheelScrollUp        { focus-column-left; }
    Mod+Ctrl+Shift+WheelScrollDown { move-column-right; }
    Mod+Ctrl+Shift+WheelScrollUp   { move-column-left; }

    Mod+1 { focus-workspace 1; }
    Mod+2 { focus-workspace 2; }
    Mod+3 { focus-workspace 3; }
    Mod+4 { focus-workspace 4; }
    Mod+5 { focus-workspace 5; }
    Mod+6 { focus-workspace 6; }
    Mod+7 { focus-workspace 7; }
    Mod+8 { focus-workspace 8; }
    Mod+9 { focus-workspace 9; }
    Mod+Ctrl+1 { move-column-to-workspace 1; }
    Mod+Ctrl+2 { move-column-to-workspace 2; }
    Mod+Ctrl+3 { move-column-to-workspace 3; }
    Mod+Ctrl+4 { move-column-to-workspace 4; }
    Mod+Ctrl+5 { move-column-to-workspace 5; }
    Mod+Ctrl+6 { move-column-to-workspace 6; }
    Mod+Ctrl+7 { move-column-to-workspace 7; }
    Mod+Ctrl+8 { move-column-to-workspace 8; }
    Mod+Ctrl+9 { move-column-to-workspace 9; }

    // Alternatively, there are commands to move just a single window:
    // Mod+Ctrl+1 { move-window-to-workspace 1; }

    // Switches focus between the current and the previous workspace.
    Mod+Tab { focus-workspace-previous; }

    Mod+Comma  { consume-window-into-column; }
    Mod+Period { expel-window-from-column; }

    // There are also commands that consume or expel a single window to the side.
    Mod+Ctrl+Comma  { consume-or-expel-window-left; }
    Mod+Ctrl+Period { consume-or-expel-window-right; }

    Mod+R { switch-preset-column-width; }
    Mod+F { maximize-column; }
    Mod+Shift+F { fullscreen-window; }
    Mod+C { center-column; }

    Mod+Minus { set-column-width "-5%"; }
    Mod+Equal { set-column-width "+5%"; }

    Mod+Shift+Minus { set-window-height "-5%"; }
    Mod+Shift+Equal { set-window-height "+5%"; }
    Mod+Shift+R  {reset-window-height; }

    Mod+Alt+0        { set-column-width "1200"; }

    Print { screenshot; }
    Ctrl+Print { screenshot-screen; }
    Alt+Print { screenshot-window; }

    Mod+Shift+P { power-off-monitors; }
    Mod+Shift+Q { quit skip-confirmation=true; }
}
