#!/bin/bash

DIR=$HOME/.config/wal/colorschemes/dark/
COLORSCHEME=($(ls ${DIR}))
RANDOMCOLORSCHEME=${COLORSCHEME[ $RANDOM % ${#COLORSCHEME[@]} ]}

wal --theme ${DIR}/${RANDOMCOLORSCHEME}

if [[ $(pidof waybar) ]]; then
   killall -SIGUSR2 waybar
fi

/home/kkm/.config/mako/update-theme.sh
notify-send -i ${DIR}/${RANDOMCOLORSCHEME} "Colorscheme Changed" ${RANDOMCOLORSCHEME}
