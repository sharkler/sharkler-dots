#!/bin/bash

wal -q -n --iterative -i ~/Pictures/bgrounds

/home/kkm/.config/mako/update-theme.sh

swww init && swww img "$(< "${HOME}/.cache/wal/wal")"

waybar -c ~/.config/hypr/waybar/config -s ~/.config/hypr/waybar/style.css &
