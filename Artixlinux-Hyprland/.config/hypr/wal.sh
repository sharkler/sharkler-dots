#!/bin/bash

wal -q -n --iterative -i ~/Pictures/bgrounds

if [[ $(pidof waybar) ]]; then
  killall -SIGUSR2 waybar
fi

swww img "$(< "${HOME}/.cache/wal/wal")"

/home/kkm/.config/mako/update-theme.sh

notify-send -i "$(< "${HOME}/.cache/wal/wal")" "Theme Changed"
