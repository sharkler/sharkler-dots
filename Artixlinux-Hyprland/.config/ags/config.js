import { Bar } from "./js/bar/Bar.js";
import { NotificationPopups } from "./js/notificationPopups.js"
import { controlpanel } from "./js/control-panel/ControlPanel.js";
import { forMonitors } from "./js/utils.js";
import { NotificationMenu } from "./mpris/NotificationMenu.js";
//import OSD from "./js/Osd/OSD.js";

App.config({
    style: "./css/style.css",
  closeWindowDelay: {
    control_panel: 300,
  },
  windows: [
    ...forMonitors(Bar),
    NotificationPopups(),
    controlpanel,
    NotificationMenu,
  ],
});

// auto reloading css
Utils.monitorFile(`/home/kkm/.config/ags/css/style.css`, () => {
  App.applyCss(`/home/kkm/.config/ags/css/style.css`);
});

