import { Workspaces } from "./Workspaces.js";
import { Clock } from "./Clock.js";
import { SysTray } from "./SysTray.js";
import { ControlButton } from "./ControlButton.js";
import { BatteryLabel } from "./BatteryLabel.js";
import CpuRam from "./cpu_ram.js";
import { Media } from "./media.js";

const StartBox = () =>
  Widget.Box({
    // vertical: true,
    hpack: "start",
    css: "margin: 0.1rem 0.1em",
    spacing: 6,
    children: [Workspaces(), Media()],
  });
const MidBox = () =>
  Widget.Box({
    // vertical: true,
    hpack: "center",
    css: "margin: 0.1rem 0.1em",
    spacing: 6,
    children: [ Clock(), BatteryLabel()],
  });

const EndBox = () =>
  Widget.Box({
    css: "margin: 0.1rem 0.1em",
    // vertical: true,
    hpack: "end",
    spacing: 6,
    children: [CpuRam(), ControlButton(), SysTray()],
  });

export const Bar = (monitor = 0) =>
  Widget.Window({
    name: `bar-${monitor}`,
    class_name: "bar",
    monitor,
    anchor: ["right", "bottom", "left"],
    exclusivity: "exclusive",
    child: Widget.CenterBox({
      class_name: "cbox",
      // vertical: true,
      start_widget: StartBox(),
      center_widget: MidBox(),
      end_widget: EndBox(),
    }),
  });
