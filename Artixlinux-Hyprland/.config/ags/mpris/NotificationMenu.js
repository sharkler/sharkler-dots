import { PopupWindow } from "../js/PopupWindow.js";
import { Media } from "./Media.js";
export const WINDOW_NAME = "NotificationMenu";
const BottomBox = () =>
  Widget.Box({
    spacing: 12,
    vertical: true,
    children: [Media()],
  });

export const NotificationMenu = PopupWindow({
  name: "NotificationMenu",
  transition: "slide_down",
  transition_duration: 300,
  anchor: ["top" ,"center"],
  margins: [4],
  keymode: "on-demand",
 child: Widget.Box({
    className: "noti-box",
    spacing: 12,
    vertical: true,
    children: [BottomBox()],
  }),
});
