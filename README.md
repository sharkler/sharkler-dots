# sharkler dots
This repository is my personal configuration backup of some wayland compositors and panels. Everyone are welcome to use this.

# dots for

[Hyprland](https://github.com/hyprwm/Hyprland)

[swayfx](https://github.com/WillPower3309/swayfx)

[wayfire](https://github.com/WayfireWM/wayfire)

[Waybar](https://github.com/Alexays/Waybar)

[labwc](https://github.com/labwc/labwc)

[niri](https://github.com/YaLTeR/niri)

[Aylur's ags](https://github.com/Aylur/ags)







## License
[GPLv3](https://www.gnu.org/licenses/gpl-3.0.html)
